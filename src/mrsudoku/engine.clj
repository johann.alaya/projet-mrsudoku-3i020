(ns mrsudoku.engine
  (:use midje.sweet)
  (:require [mrsudoku.grid :as g]))

(def ^:private sudoku-grid (var-get #'g/sudoku-grid))

(defn values
  "Return the set of values of a vector or grid `cells`."
  [cells]
  (loop [cellsi cells, set (set [])]
    (if (seq cellsi)
      (if (= (:status (first cellsi)) :empty)
        (recur (rest cellsi) set)
        (recur (rest cellsi) (conj set (:value (first cellsi)))))
      set)))

(fact
 (values (g/block sudoku-grid 1)) => #{5 3 6 9 8})

(fact
 (values (g/row sudoku-grid 1)) => #{5 3 7})

(fact
 (values (g/col sudoku-grid 1)) => #{5 6 8 4 7})

(fact
 (values (g/block sudoku-grid 8)) => #{4 1 9 8})

(fact
 (values (g/row sudoku-grid 8)) => #{4 1 9 5})

(fact
 (values (g/col sudoku-grid 8)) => #{6 8 7})

(defn values-except
  "Return the set of values of a vector of cells, except the `except`-th."
  [cells except]
  {:pre [(<= 1 except (count cells))]}
  (loop [cellsi cells, n 1, set (set [])]
    (if (seq cellsi)
      (if (= (:status (first cellsi)) :empty)
        (recur (rest cellsi) (+ n 1) set)
        (if (= n except)
          (recur (rest cellsi) (+ n 1) set)
          (recur (rest cellsi) (+ n 1) (conj set (:value (first cellsi))))))
      set)))

(fact
 (values-except (g/block sudoku-grid 1) 1) => #{3 9 6 8})

(fact
 (values-except (g/block sudoku-grid 1) 4) => #{3 9 5 8})

(defn mk-conflict [kind cx cy value]
   {:status :conflict
   :kind kind
   :value value})

(defn merge-conflict-kind
  [kind1 kind2]
  (cond
    (and (set? kind1) (set? kind2)) (clojure.set/union kind1 kind2)
    (set? kind1) (conj kind1 kind2)
    (set? kind2) (conj kind2 kind1)
    (= kind1 kind2) kind1
    :else (hash-set kind1 kind2)))

(fact
 (merge-conflict-kind :row :row) => :row)

(fact
 (merge-conflict-kind :row :block) => #{:row :block})

(fact
 (merge-conflict-kind :row #{:row :block}) => #{:row, :block})

(fact
 (merge-conflict-kind #{:row :block} :block) => #{:row, :block})

(fact
 (merge-conflict-kind #{:row :block} #{:block :col}) => #{:row :block :col})


(defn merge-conflict [conflict1 conflict2]
  (assoc conflict1 :kind (merge-conflict-kind (:kind conflict1) (:kind conflict2))))

(defn merge-conflicts [& conflicts]
  (apply (partial merge-with merge-conflict) conflicts)) 

(defn update-conflicts
  [conflict-kind cx cy value conflicts]
  (if-let [conflict (get conflicts [cx, cy])]
    (assoc conflicts [cx, cy] (mk-conflict (merge-conflict-kind conflict-kind (:kind conflict))
                                           cx cy value))
    (assoc conflicts [cx, cy] (mk-conflict conflict-kind cx cy value))))

(defn conflict-value [values except cell]
  (when-let [value (g/cell-value cell)]
    (when (and (not= (:status cell) :init)
               (contains? (values-except values except) value))
      value)))

(defn find-conflicts [s n]
  (let [val (:value (nth s n))]
    (loop [i 0, list []]
      (if (< i (count s))
        (if (and (= (:status (nth s i)) :set) (= (:value (nth s i)) val))
          (recur (+ i 1) (conj list (+ i 1)))
          (recur (+ i 1) list))
        list))))

(defn conflict-map-x [l cy val]
  (loop [li l, xmap {}]
    (if (seq li)
      (recur (next li) (assoc xmap [(first li) cy] (mk-conflict :row (first li) cy val)))
      xmap)))

(defn conflict-map-y [l cx val]
  (loop [li l, ymap {}]
    (if (seq li)
      (recur (next li) (assoc ymap [cx (first li)] (mk-conflict :col cx (first li) val)))
      ymap)))

(defn row-conflicts
  "Returns a map of conflicts in a `row`."
  [row cy]
  (loop [x 0, map {}]
    (if (< x (count row))
      (if (= (:status (nth row x)) :empty)
        (recur (+ x 1) map)
        (if (contains? (values-except row (+ x 1)) (:value (nth row x)))
          (let [l (find-conflicts row x), val (:value (nth row x))]
            (recur (+ x 1) (merge map (conflict-map-x l cy val))))
          (recur (+ x 1) map)))
      map)))

(fact
 (row-conflicts (map #(g/mk-cell :set %) [1 2 3 4]) 1) => {})

(fact
 (row-conflicts (map #(g/mk-cell :set %) [1 2 3 1]) 1)
 => {[1 1] {:status :conflict, :kind :row, :value 1},
     [4 1] {:status :conflict, :kind :row, :value 1}})

(fact
 (row-conflicts [{:status :init, :value 8} {:status :empty} {:status :empty} {:status :empty} {:status :init, :value 6} {:status :set, :value 6} {:status :empty} {:status :empty} {:status :init, :value 3}] 4)
 => {[5 4] {:status :conflict, :kind :row, :value 6}
     [6 4] {:status :conflict, :kind :row, :value 6}})

(defn rows-conflicts [grid]
  (reduce merge-conflicts {}
          (map (fn [r] (row-conflicts (g/row grid r) r)) (range 1 10))))

(defn col-conflicts
  "Returns a map of conflicts in a `col`."
  [col cx]
  (loop [y 0, map {}]
    (if (< y (count col))
      (if (= (:status (nth col y)) :empty)
        (recur (+ y 1) map)
        (if (contains? (values-except col (+ y 1)) (:value (nth col y)))
          (let [l (find-conflicts col y), val (:value (nth col y))]
            (recur (+ y 1) (merge map (conflict-map-y l cx val))))
          (recur (+ y 1) map)))
      map)))

(fact
 (col-conflicts (map #(g/mk-cell :set %) [1 2 3 4]) 1) => {})

(fact
 (col-conflicts (map #(g/mk-cell :set %) [1 2 3 1]) 1)
 => {[1 1] {:status :conflict, :kind :col, :value 1}
     [1 4] {:status :conflict, :kind :col, :value 1}})

(fact
 (col-conflicts [{:status :set, :value 8} {:status :empty} {:status :set, :value 8} {:status :set, :value 8} {:status :set, :value 6} {:status :set, :value 6} {:status :empty} {:status :empty} {:status :init, :value 3}] 4)
 => {[4 1] {:status :conflict, :kind :col, :value 8}
     [4 3] {:status :conflict, :kind :col, :value 8}
     [4 4] {:status :conflict, :kind :col, :value 8}
     [4 5] {:status :conflict, :kind :col, :value 6}
     [4 6] {:status :conflict, :kind :col, :value 6}})

(defn cols-conflicts
  [grid] (reduce merge-conflicts {}
                 (map (fn [c] (col-conflicts (g/col grid c) c)) (range 1 10))))

(defn conflict-map-b [l b val]
  (loop [li l, bmap {}]
    (if (seq li)
      (let [x (+ (mod (- (first li) 1) 3) 1), y (+ 1 (quot (- (first li) 1) 3))]
        (recur (next li) (assoc bmap [(+ x (* (mod (- b 1) 3) 3)) (+ y (* (quot (- b 1) 3) 3))] (mk-conflict :block x y val))))
      bmap)))

(mod 8 3)

(defn block-conflicts
  [block b]
  (loop [i 0, map {}]
    (if (< i (count block))
      (if (= (:status (nth block i)) :empty)
        (recur (+ i 1) map)
        (if (contains? (values-except block (+ i 1)) (:value (nth block i)))
          (let [l (find-conflicts block i), val (:value (nth block i))]
            (recur (+ i 1) (merge map (conflict-map-b l b val))))
          (recur (+ i 1) map)))
      map)))

(fact
 (block-conflicts (g/block sudoku-grid 1) 1) => {})

(fact
 (block-conflicts (map #(g/mk-cell :set %) [1 1 2
                                            3 4 5
                                            6 7 8]) 1) => {[1 1] {:status :conflict, :kind :block, :value 1}
                                                          [2 1] {:status :conflict, :kind :block, :value 1}})

(fact
 (block-conflicts (map #(g/mk-cell :set %) [1 1 2
                                            3 4 5
                                            6 7 8]) 2) => {[4 1] {:status :conflict, :kind :block, :value 1}
                                                           [5 1] {:status :conflict, :kind :block, :value 1}})

(fact
 (block-conflicts (map #(g/mk-cell :set %) [1 1 2
                                            3 4 5
                                            6 7 8]) 4) => {[1 4] {:status :conflict, :kind :block, :value 1}
                                                           [2 4] {:status :conflict, :kind :block, :value 1}})

(fact
 (block-conflicts (map #(g/mk-cell :set %) [1 1 2
                                            3 4 5
                                            6 7 8]) 9) => {[7 7] {:status :conflict, :kind :block, :value 1}
                                                           [8 7] {:status :conflict, :kind :block, :value 1}})

(fact
 (block-conflicts (map #(g/mk-cell :set %) [1 4 3
                                            3 4 5
                                            6 7 8]) 9) => {[8 7] {:status :conflict, :kind :block, :value 4}
                                                           [8 8] {:status :conflict, :kind :block, :value 4}
                                                           [9 7] {:status :conflict, :kind :block, :value 3}
                                                           [7 8] {:status :conflict, :kind :block, :value 3}})

(defn blocks-conflicts
  [grid]
  (reduce merge-conflicts {}
          (map (fn [b] (block-conflicts (g/block grid b) b)) (range 1 10))))

(defn grid-conflicts
  "Compute all conflicts in the Sudoku grid."
  [grid]
  (merge-conflicts (rows-conflicts grid)
                   (cols-conflicts grid)
                   (blocks-conflicts grid)))
